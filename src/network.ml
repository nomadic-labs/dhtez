(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_p2p
open Tezos_p2p_services
open Messages

let init_node ~config ~limits ~msg_config =
  let peer_metadata_cfg : _ P2p_params.peer_meta_config =
    {
      peer_meta_encoding = Peer_metadata.encoding;
      peer_meta_initial = Peer_metadata.empty;
      score = Peer_metadata.score;
    }
  in
  let connection_metadata_cfg cfg : _ P2p_params.conn_meta_config =
    {
      conn_meta_encoding = Connection_metadata.encoding;
      private_node = (fun { private_node; _ } -> private_node);
      conn_meta_value = (fun () -> cfg);
    }
  in
  let init_connection_metadata opt disable_mempool =
    let open Connection_metadata in
    match opt with
    | None -> { disable_mempool = false; private_node = false }
    | Some c -> { disable_mempool; private_node = c.P2p.private_mode }
  in
  P2p.create ~config ~limits peer_metadata_cfg
    (connection_metadata_cfg (init_connection_metadata (Some config) true))
    msg_config

let connect_res ~net ~net_address:(ip_addr, port) =
  let open Base in
  (* Checks if the connection is already present in the pool
     of active connections. *)
  match P2p.find_connection_by_point net (ip_addr, port) with
  | None ->
      P2p.connect
        ~timeout:
          (Ptime.to_span
          @@ Option.value_map (Ptime.of_float_s 10.)
               ~f:(fun t -> t)
               ~default:Ptime.min)
        net (ip_addr, port)
  | Some c -> Lwt_result.return c

let connect ~net ~net_address:(ip_addr, port) =
  let open Lwt.Infix in
  connect_res ~net ~net_address:(ip_addr, port) >>= function
  | Ok c -> Lwt.return_some c
  | Error _ -> Lwt.return_none

let connect_to_node ~net ~node:{ ip_addr; port; _ } =
  connect ~net ~net_address:(ip_addr, Base.Int32.to_int_exn port)

let connect_to_node_res ~net ~node:{ ip_addr; port; _ } =
  connect_res ~net ~net_address:(ip_addr, Base.Int32.to_int_exn port)
