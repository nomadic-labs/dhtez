(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_rpc_http
open Tezos_rpc_http_client_unix
open Tezos_p2p
open Messages

type error += P2p_pool_connections

type error += Locate_empty_result

type error += Key_not_found

let () =
  register_error_kind `Temporary ~id:"node.key_not_found" ~title:"Key not found"
    ~description:"Key node found."
    ~pp:(fun ppf () -> Format.fprintf ppf "Key node found.")
    Data_encoding.empty
    (function Key_not_found -> Some () | _ -> None)
    (fun () -> Key_not_found)

module S = struct
  let version =
    RPC_service.get_service ~description:"Get information on the node version"
      ~query:RPC_query.empty ~output:Data_encoding.int32
      RPC_path.(root / "version")

  let list =
    RPC_service.get_service ~description:"List the running P2P connection."
      ~query:RPC_query.empty
      ~output:(Data_encoding.list Data_encoding.string)
      RPC_path.(root / "network" / "connections")

  let store_bin =
    RPC_service.post_service ~description:"Store a binary entry in the DHT."
      ~query:RPC_query.empty
      ~input:Data_encoding.(tup4 int32 int32 Node_id.encoding Variable.bytes)
      ~output:Data_encoding.string
      RPC_path.(root / "node" / "store_bin")

  let prepare =
    RPC_service.post_service
      ~description:"Store a binary entry in the node's local buffer."
      ~query:RPC_query.empty ~input:Data_encoding.Variable.bytes
      ~output:Data_encoding.empty
      RPC_path.(root / "node" / "prepare")

  let trigger =
    RPC_service.get_service
      ~description:
        "Send the content of the node's local buffer to neighbouring nodes."
      ~query:RPC_query.empty ~output:Data_encoding.string
      RPC_path.(root / "node" / "trigger")

  let _show_store =
    RPC_service.get_service ~description:"Show key value pairs stored in node."
      ~query:RPC_query.empty
      ~output:(Data_encoding.list Node_id.encoding)
      RPC_path.(root / "node" / "show_store")

  let get =
    RPC_service.get_service ~description:"Get value associated to key."
      ~query:RPC_query.empty ~output:Data_encoding.string
      RPC_path.(root / "node" / "get" /: RPC_arg.string)

  (*let get_network =
    RPC_service.get_service
      ~description:"Get value associated to key in the network."
      ~query:RPC_query.empty ~output:Data_encoding.string
      RPC_path.(root / "network" / "get" /: RPC_arg.string)*)

  let get_network_chunk =
    RPC_service.post_service ~query:RPC_query.empty ~output:Data_encoding.bytes
      ~input:Data_encoding.(tup2 P2p_point.Id.encoding int32)
      RPC_path.(root / "network" / "get" /: RPC_arg.string)

  let locate_value_network =
    RPC_service.get_service ~query:RPC_query.empty
      ~output:
        Data_encoding.(
          obj2
            (req "packets_amount" int32)
            (req "contact_info" P2p_point.Id.encoding))
      RPC_path.(root / "network" / "locate_value" /: RPC_arg.string)

  let stored_nodes =
    RPC_service.get_service ~description:"" ~query:RPC_query.empty
      ~output:
        Data_encoding.(
          list
            (obj2 (req "pos" int32)
               (req "nodes" (list Routing.Node_info.encoding))))
      RPC_path.(root / "node" / "routing_table")

  let refresh =
    RPC_service.get_service ~description:"" ~query:RPC_query.empty
      ~output:Data_encoding.empty
      RPC_path.(root / "node" / "refresh")
end

let rpc_directory t =
  let dir = RPC_directory.empty in
  let version : int32 = Int32.of_int 1 in
  let dir =
    RPC_directory.gen_register dir S.version (fun () () () ->
        RPC_answer.return version)
  in
  let dir =
    RPC_directory.register0 dir S.list (fun () () ->
        let open Proto in
        match P2p.pool t.net with
        | None -> fail P2p_pool_connections
        | Some pool ->
            return
            @@ P2p_pool.Connection.fold pool ~init:[] ~f:(fun peer_id _ acc ->
                   (peer_id |> P2p_peer.Id.to_b58check) :: acc))
  in
  let dir =
    RPC_directory.register dir S.store_bin (fun _ _ (n, total, target, value) ->
        Proto.store t n total target value)
  in
  let dir =
    RPC_directory.register dir S.prepare (fun _ _ v ->
        let open Base in
        let b = Bytes.create (Bytes.length v) in
        Bytes.blit ~src:v ~src_pos:0 ~dst:b ~dst_pos:0 ~len:(Bytes.length v);
        t.data <- b;
        return ())
  in
  let dir =
    RPC_directory.register dir S.trigger (fun _ _ _ ->
        let b = Bytes.create (1 lsl 10) in
        let len = min (Bytes.length b) (Bytes.length t.data) in
        Base.Bytes.blit ~src:t.data ~src_pos:0 ~dst:b ~dst_pos:0 ~len;
        let target = Node_id.hash_bytes [ b ] in
        Proto.locate_node t ~target >>=? function
        | s -> (
            match s with
            | [] -> fail Locate_empty_result
            | nodes ->
                let rec loop = function
                  | [] -> Lwt_result.return (Node_id.to_b58check target)
                  | node :: rest -> (
                      Network.connect_to_node ~net:t.net ~node >>= function
                      | Some conn -> (
                          P2p.send t.net conn
                            (Store
                               ( Proto.increment_msg_uuid t,
                                 Int32.of_int 1,
                                 Int32.of_int 1,
                                 { key = target; value = t.data } ))
                          >>= function
                          | Ok _ -> loop rest
                          | Error e ->
                              Error_monad.pp_print_error Format.err_formatter e;
                              Lwt_result.return (Node_id.to_b58check target))
                      | None -> Lwt_result.return () >>=? fun _ -> loop rest)
                in
                loop nodes))
  in
  (*let dir =
      RPC_directory.register0 dir S.show_store (fun () () ->
          return
          @@ (Base.Hashtbl.to_alist t.store |> Base.List.map ~f:(fun (k, _) -> k)))
    in*)
  let dir =
    RPC_directory.register0 dir S.refresh (fun () () -> Proto.refresh_buckets t)
  in
  let dir =
    RPC_directory.register dir S.get (fun (_, key) () () ->
        let f = Format.sprintf "./%s-%s" (Node_id.to_b58check t.info.id) key in
        match Base.Hashtbl.mem t.store (Node_id.of_string_exn key) with
        | false -> fail Key_not_found
        | true ->
            let v = Stdio.In_channel.read_all f |> Bytes.of_string in
            return @@ Bytes.to_string v)
  in
  (*let dir =
      RPC_directory.register dir S.get_network (fun (_, key) () () ->
          let target = Node_id.of_b58check_exn key in
          Proto.locate_value t ~target >>=? function
          | Key_val ((ip_addr, port), packets) -> (
              Network.connect ~net:t.net ~net_address:(ip_addr, port) >>= function
              | Some conn ->
                  let rec loop i () =
                    match i with
                    | i when Int32.(equal i zero) -> Lwt_result.return ()
                    | _ ->
                        let message_uuid = Proto.increment_msg_uuid t in
                        P2p.send t.net conn (Get_value (message_uuid, i, target))
                        >>= fun _ ->
                        Proto.wait_msg ~message_uuid ~conn
                          ~timeout:(Misc.span_of_float_sec ~n_sec:30.)
                          t
                        >>= fun _ -> loop (Int32.pred i) ()
                  in
                  loop packets () >>= fun _ -> return @@ "test"
              | None -> fail Connection_failure)
          | Closest _ -> fail Value_not_found)
    in*)
  let dir =
    RPC_directory.register dir S.locate_value_network (fun (_, key) () () ->
        Proto.locate_value t ~target:(Node_id.of_b58check_exn key))
  in
  let dir =
    RPC_directory.register dir S.get_network_chunk
      (fun ((), key) () ((ip_addr, port), i) -> Proto.get t key ip_addr port i)
  in

  RPC_directory.register0 dir S.stored_nodes (fun () () ->
      let answer =
        let rec loop i acc =
          match i with
          | 129 -> acc
          | _ -> (
              match Routing.Table.bucket t.buckets i with
              | Some nodes -> loop (i + 1) @@ ((Int32.of_int i, nodes) :: acc)
              | None -> loop (i + 1) acc)
        in
        loop 0 []
      in
      return @@ answer)

open RPC_context

let ctxt ~rpc_addr ~rpc_port =
  let endpoint =
    Uri.of_string @@ Format.sprintf "http://%s:%d" rpc_addr rpc_port
  in
  new Tezos_rpc_http_client_unix.RPC_client_unix.http_ctxt
    { RPC_client_unix.default_config with endpoint }
    Media_type.all_media_types

let store ~rpc_addr ~rpc_port n total target data =
  make_call S.store_bin (ctxt ~rpc_addr ~rpc_port) () () (n, total, target, data)

let locate_value ~rpc_addr ~rpc_port key =
  make_call1 S.locate_value_network (ctxt ~rpc_addr ~rpc_port) key () ()

let get_chunk ~rpc_addr ~rpc_port key contact i =
  make_call S.get_network_chunk (ctxt ~rpc_addr ~rpc_port) ((), key) ()
    (contact, i)
