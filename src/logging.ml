(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base
open Tezos_event_logging
open Messages

let pp_node fmt { id; ip_addr; port } =
  Format.fprintf fmt "%s@%s:%ld" (Node_id.to_b58check id)
    (P2p_addr.to_string ip_addr)
    port

let rec pp_list pp fmt = function
  | [] -> ()
  | [ x ] -> Format.fprintf fmt "%a" pp x
  | x :: xs -> Format.fprintf fmt "%a,\n%a" pp x (pp_list pp) xs

let pp_find_value fmt = function
  | Value _ -> Format.fprintf fmt "Value"
  | Closest_nodes n -> pp_list pp_node fmt n

module Kademlia = struct
  include Internal_event.Simple

  let section = [ "kademlia" ]

  let node_ready =
    declare_1 ~section ~name:"node_ready" ~msg:"Node ready (port: {port})@."
      ~level:Info
      ("port", Data_encoding.uint16)

  let received_ping =
    declare_1 ~section ~name:"received_ping"
      ~msg:"Received Ping from {Node_id.t}" ~level:Notice
      ("Node_id.t", P2p_peer.Id.encoding)

  let received_ack =
    declare_1 ~section ~name:"received_ack" ~msg:"Received Ack from {sender}"
      ~level:Notice ("sender", Encoding.node) ~pp1:pp_node

  let join =
    declare_0 ~section ~name:"join" ~msg:"Joined network" ~level:Notice ()

  let error = declare_0 ~section ~name:"error" ~msg:"error" ~level:Notice ()

  let show_peer_id =
    declare_1 ~section ~name:"show_peer_id" ~msg:"My peer ID is {peer_id}"
      ~level:Notice
      ("peer_id", P2p_peer.Id.encoding)

  let store =
    declare_1 ~section ~name:"store" ~msg:"Stored value with key '{key}'"
      ~level:Notice ("key", Node_id.encoding)

  let find_node =
    declare_2 ~section ~name:"find_node"
      ~msg:"Received Find_node ({node}) from {sender}" ~level:Notice
      ("node", Node_id.encoding) ("sender", Encoding.node) ~pp2:pp_node

  let find_node_response =
    declare_2 ~section ~name:"find_node_response"
      ~msg:"Received Find_node_response ({nodes}) from {sender}" ~level:Notice
      ("nodes", Data_encoding.list Encoding.node)
      ("sender", Encoding.node) ~pp1:(pp_list pp_node) ~pp2:pp_node

  let find_value =
    declare_2 ~section ~name:"find_value"
      ~msg:"Received Find_value ({value}) from {sender}" ~level:Notice
      ("value", Node_id.encoding)
      ("sender", Encoding.node)

  let find_value_response =
    declare_2 ~section ~name:"find_value_response"
      ~msg:"Received Find_value_response({nodes}) from {sender}" ~level:Notice
      ("nodes", Encoding.find_value)
      ("sender", Encoding.node) ~pp1:pp_find_value ~pp2:pp_node

  let bye =
    (* Note that "exit_code" may be negative in case of signals. *)
    declare_1 ~section ~name:"bye" ~msg:"bye" ~level:Notice
      (* may be negative in case of signals *)
      ("exit_code", Data_encoding.int31)

  let shutting_down =
    declare_0 ~section ~name:"shutting_down_node" ~msg:"Shutting down"
      ~level:Info ()
end

module Rpc = struct
  include Internal_event.Simple

  let section = [ "RPC" ]

  let shutting_down_rpc_server =
    declare_0 ~section ~name:"shutting_down_rpc_server" ~msg:"Shutting down"
      ~level:Notice ()
end
