(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base
open Tezos_p2p

module Ip_addr : sig
  include module type of P2p_addr

  val sexp_of_t : t -> Ppx_sexp_conv_lib.Sexp.t

  val compare : t -> t -> int
end

module Int32 : sig
  include module type of Int32

  val sexp_of_t : t -> Ppx_sexp_conv_lib.Sexp.t
end

type node = { id : Node_id.t; ip_addr : Ip_addr.t; port : Int32.t }
[@@deriving sexp_of, compare]

val node_equal : node -> node -> bool

(** Kademlia messages, beginning with their sequence number. *)
type message =
  | Ping of int64  (** Probes a node to check if it is online. *)
  | Ack of int64  (** Ping response, to prove the node is up. *)
  | Store of int64 * int32 * int32 * key_value
      (** Instructs a node to store a key-value pair for later retrieval. *)
  (* TODO: Why not adding the possibility to make data expire after some time?
     24 hours or longer for certificates. *)
  | Find_node of int64 * Node_id.t
      (** Ask for the closest nodes to a node id. *)
  | Find_node_response of int64 * node list
      (** Returns data for the closest nodes to the node id. *)
  | Find_value of int64 * Node_id.t
      (** Query node for a value with a given digest. *)
  | Find_value_response of int64 * find_value
      (** Either returns the file size of the queried file if present in 
        the local store, or the closest nodes to the key. *)
  | Get_value of int64 * int32 * Node_id.t
      (** Asks a node for a i-th chunk for a specific file. *)
  | Get_value_response of int64 * key_value
      (** Get the specified chunk. TODO: tell if the chunk is not found. *)

and key_value = { key : Node_id.t; value : bytes }

and find_value = Closest_nodes of node list | Value of int32

module Encoding : sig
  val key_value : key_value Data_encoding.t

  val node : node Data_encoding.t

  val find_value : find_value Data_encoding.t
end

val msg_config : message P2p_params.message_config

type metadata = Metadata
