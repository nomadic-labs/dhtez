(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_p2p
open Tezos_p2p_services

(** {1 The state of a node} *)

type t = {
  net : (Messages.message, Peer_metadata.t, Connection_metadata.t) P2p.t;
      (** The network type, parametrized by the Kademlia [Messages.message] type.
      Peers can only send messages of this type. *)
  bootstrap : Tezos_base.P2p_point.Id.t option;
  rpc_listening : P2p_point.Id.t;
  info : Messages.node;
      (** The current node contact information: node ID and network address (IP, TCP port). *)
  buckets : Routing.Table.t;  (** The routing table of the current node. *)
  store : (Node_id.t, string) Base.Hashtbl.t;
      (** File paths to the stored files indexed by their digest. *)
  pending_queries : (int64, Messages.message Lwt_condition.t) Base.Hashtbl.t;
      (** Kademlia queries waiting to be resolved. *)
  active_connections : (Node_id.t, int64) Base.Hashtbl.t;
      (** The count of pending queries for active connections, indexed by the node IDs. *)
  pending_files : (Node_id.t, int32) Base.Hashtbl.t;
      (** Files that are being processed, the ID corresponds to the digest of the file being
  uploaded and the integer to the chunk number. *)
  mutable data : bytes;
  mutable found_bootstrap : Routing.Node.t option;
      (** Whether the current node has found its bootstrapping node. *)
  mutable msg_uuid : int64;
      (** The last message sent sequence number (uuid stands for unique universal ID ). *)
  mutable joined : bool;  (** Whether the current node has joined the network. *)
}
(** The current instance of a node, comprising:
    - its ID and network address
    - a hash table responsible of storing part of the contents of the DHT
    - its routing table, organized in 'k-buckets':
    buckets of length k holding the contacts of the current node instance.

    The k-buckets verify the following property:
    every contact in bucket [0<=i<k] is such that [2^i <= node xor contact < 2^(i+1)].
    *)

(** {1 Node and value lookup} *)

val locate_node : t -> target:Node_id.t -> Messages.node list tzresult Lwt.t
(** Attempts to locate a node with node id [target] in the network [net]. *)

val locate_value :
  t -> target:Node_id.t -> (int32 * (Messages.Ip_addr.t * int)) tzresult Lwt.t
(** Attempts to find a value [target] in the network [net]. *)

val refresh_buckets : t -> unit tzresult Lwt.t
(** The node selects a random number in each range and does a refresh,
a node lookup using that number as key. *)

type error += Bootstrap_failure

val join : t -> bootstrap:P2p_addr.t * int -> unit tzresult Lwt.t
(** Join the network using [bootstrap]. *)

(** {1 Processing messages} *)

val wait_msg :
  conn:(Messages.message, Peer_metadata.t, Connection_metadata.t) P2p.connection ->
  message_uuid:int64 ->
  timeout:Ptime.span ->
  t ->
  Messages.message tzresult Lwt.t
(** Waits for the message with ID [message_uuid] with a specified [timeout]. *)

val resolve :
  id:Node_id.t ->
  t ->
  message_uuid:int64 ->
  msg:Messages.message ->
  'a ->
  unit Lwt.t
(** Returns the value for the query with id [message_uuid]. *)

val process_msg :
  t ->
  conn:(Messages.message, Peer_metadata.t, Connection_metadata.t) P2p.connection ->
  msg:Messages.message ->
  sender:Messages.node ->
  id:Node_id.t ->
  unit Lwt.t
(** Processes a message [msg] from a connection [conn]. *)

val increment_msg_uuid : t -> int64
(** Increments the message uuid (mod [Int64.max_int]) and returns its updated value. *)

val store_data : t -> key:Node_id.t -> value:bytes -> unit
(** Stores a key-value pair in the node. *)

type error += Locate

val store : t -> int32 -> int32 -> Node_id.t -> bytes -> string tzresult Lwt.t

val get : t -> string -> P2p_addr.t -> int -> int32 -> bytes tzresult Lwt.t
