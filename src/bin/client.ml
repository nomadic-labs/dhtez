(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives

let get_cmd =
  let open Core in
  Command.basic ~summary:"Retrieve value associated to [key]"
    Command.Let_syntax.(
      let%map_open key = anon ("key" %: string)
      and host =
        flag "--host" (required string)
          ~doc:"addr:port RPC network address of the peer"
      and destination =
        flag "--destination" (required string)
          ~doc:"path/to/destination Path to file"
      in
      fun () ->
        let rpc_host = Misc.parse_addr (Some host) |> Base.Option.value_exn in
        match Lwt_main.run (Lib_dht.get ~rpc_host ~key ~destination) with
        | Ok () -> Format.printf "Done.@."
        | Error e -> Error_monad.pp_print_error_first Format.err_formatter e)

let store_cmd =
  let open Core in
  Command.basic ~summary:"Store [file] in the DHT"
    Command.Let_syntax.(
      let%map_open path = anon ("file" %: string)
      and host =
        flag "--host" (required string)
          ~doc:"addr:port RPC network address of the peer"
      in
      fun () ->
        let rpc_host = Misc.parse_addr (Some host) |> Base.Option.value_exn in
        match Lwt_main.run (Lib_dht.store ~rpc_host path) with
        | Ok id -> Format.printf "%s@." @@ Lib_dht.Node_id.to_b58check id
        | Error e -> Error_monad.pp_print_error_first Format.err_formatter e)

let command =
  Core.Command.group ~summary:"Interact with the DHT"
    [ ("get", get_cmd); ("store", store_cmd) ]

let () = Core.Command.run command
