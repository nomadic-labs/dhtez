(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base
include P2p_peer.Id
open Base

(* Extended indexing operators *)
module Bytes = struct
  include Bytes

  let ( .%() ) = get

  let ( .%()<- ) = set
end

let in_range i = assert (0 <= i && i < 128)

let ( ^ ) x y = Char.(of_int_exn (to_int x lxor to_int y))

let ( << ) x y = Char.(of_int_exn (to_int x lsl to_int y))

let ( >> ) x y = Char.(of_int_exn (to_int x lsr to_int y))

let xor_bytes a b =
  let open Bytes in
  let len = length a in
  assert (Int.(len = length b));
  let res = create len in
  for i = 0 to len - 1 do
    res.%(i) <- a.%(i) ^ b.%(i)
  done;
  of_bytes_exn res

let xor a b = xor_bytes (to_bytes a) (to_bytes b)

let to_Z t = Z.of_bits (Hex.to_string @@ to_hex t)

let compare a b = Z.compare (to_Z a) (to_Z b)

let ( < ) a b = Int.( < ) (compare a b) 0

let ( > ) a b = Int.( > ) (compare a b) 0

let ( = ) a b = Int.( = ) (compare a b) 0

let ( <> ) a b = not @@ Int.( = ) (compare a b) 0

let sexp_of_t t = Sexp.List [ Sexp.Atom "node_id"; Sexp.Atom (to_string t) ]

let random_id () =
  let b = Bytes.create 16 in
  let (_ : bool) = Hacl_star.Hacl.RandomBuffer.randombytes b in
  of_bytes_exn @@ b

let bucket_index a b =
  match Z.log2 @@ to_Z @@ xor a b with
  | exception Invalid_argument _ -> 0
  | z -> z

let bucket_index_bytes a b =
  match Z.log2 @@ to_Z @@ xor_bytes a b with
  | exception Invalid_argument _ -> 0
  | z -> z

let random_id_at_distance node_id distance =
  in_range distance;

  let one = Char.of_int_exn 1 in

  let ( % ) a m = Char.of_int_exn (Int.rem a m) in

  let open Bytes in
  let bit_at b i =
    in_range i;
    1 land Char.to_int (b.%(i / 8) >> i % 8)
  in

  let flip_bit b i =
    in_range i;
    b.%(i / 8) <- b.%(i / 8) ^ (one << i % 8)
  in
  (* Generates a random ID. *)
  let r_id = random_id () in
  let dist_id = to_bytes @@ xor r_id node_id in
  let node_id = to_bytes node_id in
  (* Loop over the nonzero bits of the distance between the random ID
     and the given [node_id], setting bits to zero to get the
     appropriate distance. *)
  let rec loop b dist = function
    | 128 -> b
    | i when Int.(equal (bit_at dist i) 1) -> (
        flip_bit b i;
        let current_distance = bucket_index_bytes b node_id in
        match Int.compare distance current_distance with
        | 0 -> b
        | i when Int.(i > 0) ->
            flip_bit b distance;
            b
        | _ -> loop b dist (i + 1))
    | i -> loop b dist (i + 1)
  in
  loop (to_bytes r_id) dist_id 0 |> of_bytes_exn
