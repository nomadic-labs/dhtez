(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base
open Tezos_p2p

(* Let's extend some modules in order to add some serialization capabilities. *)

module Ip_addr = struct
  include P2p_addr

  let sexp_of_t ip_addr =
    Base.(Sexp.List [ Sexp.Atom "ip_addr"; Sexp.Atom (to_string ip_addr) ])

  let compare x y = String.compare (to_string x) (to_string y)

  let equal x y = compare x y = 0
end

module Int32 = struct
  include Int32

  let sexp_of_t i =
    Base.(Sexp.List [ Sexp.Atom "int32"; Sexp.Atom (to_string i) ])
end

type node = { id : Node_id.t; ip_addr : Ip_addr.t; port : Int32.t }
[@@deriving sexp_of, compare, equal]

let node_equal (x : node) (y : node) = compare x y = 0

type message =
  | Ping of int64
  | Ack of int64
  | Store of int64 * int32 * int32 * key_value
  | Find_node of int64 * Node_id.t
  | Find_node_response of int64 * node list
  | Find_value of int64 * Node_id.t
  | Find_value_response of int64 * find_value
  | Get_value of int64 * int32 * Node_id.t
  | Get_value_response of int64 * key_value

and find_value = Closest_nodes of node list | Value of int32

and key_value = { key : Node_id.t; value : bytes }

(* Let's define the encoding for types which are sent across the network. *)
module Encoding = struct
  let key_value =
    let open Data_encoding in
    conv
      (fun { key; value } -> (key, value))
      (fun (key, value) -> { key; value })
      (obj2 (req "key" Node_id.encoding) (req "value" Variable.bytes))

  let node =
    let open Data_encoding in
    conv
      (fun ({ id; ip_addr; port } : node) -> (id, ip_addr, port))
      (fun (id, ip_addr, port) -> { id; ip_addr; port })
      (obj3
         (req "id" P2p_peer.Id.encoding)
         (req "ip_addr" P2p_addr.encoding)
         (req "port" int32))

  let find_value =
    let open Data_encoding in
    dynamic_size
    @@ union ~tag_size:`Uint8
         [
           case (Tag 0x17) ~title:"Closest_nodes"
             (obj2
                (req "nodes"
                   (Variable.list ~max_length:Constants.bucket_size node))
                (req "kind" (constant "Closest_nodes")))
             (function Closest_nodes n -> Some (n, ()) | _ -> None)
             (fun (n, ()) -> Closest_nodes n);
           case (Tag 0x18) ~title:"Value"
             (obj2 (req "value" int32) (req "kind" (constant "value")))
             (function Value packets -> Some (packets, ()) | _ -> None)
             (fun (packets, ()) -> Value packets);
         ]
end

open Encoding
open Data_encoding

let msg_config : message P2p_params.message_config =
  {
    encoding =
      [
        P2p_params.Encoding
          {
            tag = 0x10;
            title = "Ping";
            encoding = int64;
            wrap = (function i -> Ping i);
            unwrap = (function Ping i -> Some i | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x11;
            title = "Ack";
            encoding = int64;
            wrap = (function i -> Ack i);
            unwrap = (function Ack i -> Some i | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x12;
            title = "Store";
            encoding =
              dynamic_size ~kind:`Uint30 @@ tup4 int64 int32 int32 key_value;
            wrap =
              (function
              | i, n, total, key_value -> Store (i, n, total, key_value));
            unwrap =
              (function
              | Store (i, n, total, key_value) -> Some (i, n, total, key_value)
              | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x13;
            title = "Find_node";
            encoding = tup2 int64 P2p_peer.Id.encoding;
            wrap = (function message_uuid, n -> Find_node (message_uuid, n));
            unwrap =
              (function
              | Find_node (message_uuid, n) -> Some (message_uuid, n)
              | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x14;
            title = "Find_node_response";
            encoding = tup2 int64 @@ list node;
            wrap =
              (function
              | message_uuid, nodes -> Find_node_response (message_uuid, nodes));
            unwrap =
              (function
              | Find_node_response (message_uuid, nodes) ->
                  Some (message_uuid, nodes)
              | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x15;
            title = "Find_value";
            encoding = tup2 int64 Node_id.encoding;
            wrap = (function i, key -> Find_value (i, key));
            unwrap =
              (function Find_value (i, key) -> Some (i, key) | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x16;
            title = "Find_value_response";
            encoding = tup2 int64 find_value;
            wrap = (function i, response -> Find_value_response (i, response));
            unwrap =
              (function
              | Find_value_response (i, response) -> Some (i, response)
              | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x1A;
            title = "Get_value";
            encoding = tup3 int64 int32 Node_id.encoding;
            wrap = (function uuid, i, key -> Get_value (uuid, i, key));
            unwrap =
              (function
              | Get_value (uuid, i, key) -> Some (uuid, i, key) | _ -> None);
            max_length = None;
          };
        P2p_params.Encoding
          {
            tag = 0x1B;
            title = "Get_value_response";
            encoding = tup2 int64 key_value;
            wrap = (function i, response -> Get_value_response (i, response));
            unwrap =
              (function
              | Get_value_response (i, response) -> Some (i, response)
              | _ -> None);
            max_length = None;
          };
      ];
    chain_name = Distributed_db_version.Name.of_string "DHT";
    distributed_db_versions = Distributed_db_version.[ zero ];
  }

type metadata = Metadata
