
all:
	BISECT_FILE=$(pwd)/_coverage/ dune build --instrument-with bisect_ppx
	rm -f ./node ./client
	cp _build/install/default/bin/* .

debug:
	TEZOS_LOG="p2p.io-scheduler->info; p2p.fd->info; p2p.*->debug; rpc->debug;" dune exec src/bin/node.exe --no-buffer -- --listening $(listening) --bootstrap $(bootstrap)

make test:
	sh test.sh

clean:
	dune clean
	git clean -dfX
	rm -rf _docs/

.PHONY : doc
doc:
	rm -rf _docs/
	mkdir -p _docs/
	dune build @doc
	cp	-r ./_build/default/_doc/_html/* _docs/

format:
	dune build @fmt --auto-promote

coverage:
	make clean
	BISECT_ENABLE=yes dune build
	dune runtest
	bisect-ppx-report html
	bisect-ppx-report summary

refresh-pin:
	cd tezos && eval $(opam env) && make && sh ./scripts/opam-pin.sh
