Online documentation at: [https://jtcoolen.gitlab.io/p2p_node/lib_dht/index.html](https://jtcoolen.gitlab.io/p2p_node/lib_dht/index.html).

Note: Check out this branch `no_chunks` to use an earlier version of the DHT that encodes a files in a single message.

First, make sure to install the following dependencies:
rsync git m4 patch unzip wget pkg-config libgmp-dev libev-dev libhidapi-dev libffi-dev opam jq zlib1g-dev libssl-dev
Install the rust toolchain v1.44.0.

TL;DR on Ubuntu:
```
sudo apt install -y apt-utils
sudo apt install -y rsync git m4 build-essential patch unzip wget pkg-config libgmp-dev libev-dev libhidapi-dev libffi-dev opam jq zlib1g-dev libssl-dev
wget https://sh.rustup.rs/rustup-init.sh
chmod +x rustup-init.sh
./rustup-init.sh --profile minimal --default-toolchain 1.44.0 -y
source $HOME/.cargo/env
```

Then, set up the OCaml environment with `sh install.sh`.

**Do not forget to run `eval $(opam env)` if you are in an other switch.**

Build the executable with `make`.

Example requests:
```
$> node
Aug 30 13:28:29.239 - kademlia: My peer ID is idt2jH97EzPij3hGFLXiNF6akN5ngn
...
$> client store /path/to/file -host x.x.x.x:x
Key: idtdUW9fxSD5Z9ndmT4YpCpc9fJCeB

$> client get idtp5s1FVzxqiuxgJ1DnNb6AWWqhLh -host x.x.x.x:x -dest path
```
