(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let ping ~rpc_port ~path () : Data_encoding.json option Lwt.t =
  let open Lwt.Infix in
  RPC_client_unix.generic_json_call `GET
    (Uri.of_string @@ Format.sprintf "http://localhost:%d/%s" rpc_port path)
  >>= function
  | Ok (`Ok o) ->
      Format.eprintf "Got %s@." @@ Data_encoding.Json.to_string o;
      Lwt.return_some o
  | _ -> Lwt.return_none

let refresh ~rpc_port =
  let open Lwt.Infix in
  RPC_client_unix.generic_call `GET
    ~accept:[ Tezos_rpc_http.Media_type.json ]
    (Uri.of_string @@ Format.sprintf "http://localhost:%d/node/refresh" rpc_port)
  >>= function
  | Ok (`Ok (body, _, _)) -> Cohttp_lwt.Body.to_string body >|= Option.some
  | Error e ->
      Tezos_error_monad.Error_monad.pp_print_error Format.err_formatter e;
      Lwt.return_some "argh"
  | _ -> Lwt.return_none

let rand_pick points =
  Random.self_init ();
  List.nth points @@ Random.int (List.length points)

let mk_point points i net_port =
  let bootstrap = rand_pick points in
  Node.init
    ~name:(Format.sprintf "node%d" i)
    ~net_addr:(Format.sprintf ":::%d" net_port)
    ~rpc_addr:(Some (Format.sprintf "::ffff:127.0.0.1:%d" (net_port + 20000)))
    ~bootstrap_addr:(Some (Format.sprintf "::1:%d" bootstrap))
    ?color:None ?event_level:None ?runner:None ?event_pipe:None ()

let rand_port points =
  let rec loop i = function
    | true -> i
    | false ->
        let n = 10000 + Random.int 220 in
        loop n (not @@ List.mem n points)
  in
  loop 0 false

let wait_for_join node = Node.wait_for node "join.v0" @@ fun _ -> Some ()

let mk_network n acc =
  Random.self_init ();
  let rec loop acc = function
    | 0 -> Lwt.return acc
    | i ->
        let net_port = rand_port acc in
        let* n = mk_point acc i net_port in
        (*let _ = Node.log_events n in*)
        let* () = wait_for_join n in
        loop (net_port :: acc) (i - 1)
  in
  loop acc n

let delete_last_char s = String.sub s 0 (String.length s - 1)

let check_node_initialization () =
  Test.register ~__FILE__ ~title:"p2p" ~tags:[ "p2p"; "node" ] @@ fun () ->
  (*let* node = Node.init [History_mode history_mode] in
    let* client = Client.init ~endpoint:(Node node) () in
    let* () = Client.activate_protocol ~protocol client in*)
  Log.info "Activated protocol.";
  Check.( < ) 1 2 Check.int ~error_msg:"failure";
  let* _node =
    Node.init ~name:"node0" ~net_addr:"::1:10000"
      ~rpc_addr:(Some (Format.sprintf "::ffff:127.0.0.1:%d" 30000))
      (*~bootstrap_addr:(Some (Format.sprintf "65.21.177.245:%d" 10000))*)
      ()
  in
  let* () = Lwt_unix.sleep 1. in
  (*Node.log_events _node;*)
  let* net = mk_network 20 [ 10000 ] in
  let path = "./README.md" in

  let _data =
    let chan = open_in path in
    let size = 4 * 1024 in
    let buffer = Bytes.create size in
    let len = input chan buffer 0 size in
    Bytes.sub buffer 0 len |> Bytes.to_string
  in

  let store_port = rand_pick net + 20000 in
  let* s =
    Client.store "./README.md" ~host:(Format.sprintf "127.0.0.1:%d" store_port)
  in
  Format.eprintf "Key: %s@." s;

  let key = delete_last_char s in
  let* () = Lwt_unix.sleep 5. in

  let rec loop f = function
    | [] -> Lwt.return_unit
    | rpc_port :: rest ->
        let open Lwt.Infix in
        f rpc_port >>= fun _ -> loop f rest
  in
  let* _ =
    loop
      (fun rpc_port ->
        Log.info "Querying on port %d" rpc_port;
        let dest = Format.sprintf "%d" (rpc_port - 20000) in
        let* response =
          Client.get key ~host:(Format.sprintf "127.0.0.1:%d" rpc_port) ~dest
        in
        let response = delete_last_char response in
        let digest = Lib_dht.Node_id.(digest dest |> to_b58check) in
        Check.( = ) response "Done." Check.string
          ~error_msg:"Could not retrieve value.";
        Check.( = ) digest key Check.string ~error_msg:"Data integrity error.";
        Lwt.return_unit)
      (List.map (fun i -> i + 20000) net)
  in
  return ()

let register = check_node_initialization
